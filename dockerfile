FROM tiangolo/uwsgi-nginx-flask:python3.8

WORKDIR /app
RUN apt-get update && apt-get install -y git
RUN git config --global http.sslverify false
RUN git clone https://gitlab.com/tfgggg/Auto-Back.git
RUN cp -a Auto-Back/. /app/
RUN pip install flask
RUN pip install flask_cors
RUN pip install flask_sqlalchemy
RUN pip install flask_marshmallow

