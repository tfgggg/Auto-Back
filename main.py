from flask import Flask,jsonify
import sqlite3,json,os
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

app = Flask(__name__)
db_url = os.getenv('db_url')
if(db_url):
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+ db_url
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/huanglizhen/Documents/ArchadeNet_Auto/db/Auto.db'

CORS(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Log(db.Model):
  
    Id = db.Column(db.String)
    TimeStamp =db.Column(db.String,primary_key=True)
    ICase =db.Column(db.String)
    Source =db.Column(db.String)
    LogLevel = db.Column(db.Integer)
    LogLevelName =db.Column(db.String)
    Message =db.Column(db.String)
    Args =db.Column(db.String)
    Module =db.Column(db.String)
    FuncName =db.Column(db.String)
    LineNo = db.Column(db.Integer)
    Exception =db.Column(db.String)
    Process = db.Column(db.Integer)
    Thread =db.Column(db.String)
    ThreadName =db.Column(db.String)
    
    def __init__(self,id):
        self.id = id


class LogSchema(ma.Schema):
    class Meta:
        fields = ("Id","TimeStamp","ICase","Message","Module","FuncName")

class Run(db.Model):
  
    Id = db.Column(db.String,primary_key=True)
    TimeStamp =db.Column(db.String)
    Suite =db.Column(db.String)

class RunSchema(ma.Schema):
    class Meta:
        fields = ("Id","TimeStamp","Suite")


@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/report/<string:id>', methods=["GET"])
def report(id):
    logresult = Log.query.filter(Log.Id == id,Log.FuncName!="setUp")
    logschema = LogSchema(many=True)
    return jsonify(logschema.dumps(logresult))

@app.route('/testname/<string:id>', methods=["GET"])
def testname(id):
    query = db.session.query(Log.ICase.distinct().label('ICase'),Log.Module.label("Module")).filter(Log.Id==id ,Log.FuncName=="setUp")
    testcase = {}
    for row in query.all():
        if(row.Module not in testcase):
          testcase[row.Module] =  [row.ICase] 
        else:
          testcase[row.Module].append(row.ICase)

    print(testcase)
    return jsonify(testcase)

@app.route('/runs', methods=["GET"])
def run(): 
    #query = Log.query.with_entities(Log.Id,Log.TimeStamp).distinct()
    run = Run.query.filter()
    runSchema = RunSchema(many=True)
    print(run)
    return jsonify(runSchema.dumps(run))



if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=80)